﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroids_controler : MonoBehaviour
{
    public Rigidbody2D rb;

    public float force;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 playerDirection = game_manager.instance.player.transform.position - transform.position;
        rb.AddForce(playerDirection * force);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
