﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class movementscript : MonoBehaviour
{
    public float speed;
    public float maxSpeed;
    public GameObject greenAlien;

    private bool canMove =true;
    //when comparing i need to use two = signs


    // Start is called before the first frame update
    void Start()
    {
        //THIS FILE IS WORKING AS OF JUNE 20 2020 AT 1617 HOURS
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            speed = 1;
        }
        else
        {
            speed = maxSpeed;
        }

        
        if (Input.GetKey(KeyCode.Space))
        {

            Vector2 position = new Vector2(0, 0);
            transform.position = position;
        }

        if (Input.GetKeyDown(KeyCode.P))
        { if(canMove == true)
            {

                canMove = false;

            }

        else
            {

                canMove = true;

            }
                
         
         }

        if(canMove == true)
        {
            if (Input.GetKey(KeyCode.UpArrow)) //we are selecting and using the up arrow
            {


                Vector2 move = new Vector2(0, speed * Time.deltaTime);
                Vector2 position = new Vector2(transform.position.x, transform.position.y);
                position += move;
                transform.position = position;
                //the above allows the object ship or alien to move upwards when the up arrow is pressed

            }

            if (Input.GetKey(KeyCode.DownArrow))//we are selecting and using the down arrow
            {

                Vector2 move = new Vector2(0, -speed * Time.deltaTime);
                Vector2 position = new Vector2(transform.position.x, transform.position.y);
                position += move;
                transform.position = position;
                //allows the alien ship to move downwards when the down arrow is pressed


            }

            if (Input.GetKey(KeyCode.RightArrow))//we are selecting and using the right arrow
            {

                Vector2 move = new Vector2(speed * Time.deltaTime, 0);
                Vector2 position = new Vector2(transform.position.x, transform.position.y);
                position += move;
                transform.position = position;
                //Allows the alienship to move right when the right Arrow is pressed


            }

            if (Input.GetKey(KeyCode.LeftArrow))//we are selecting and using the left arrow
            {

                Vector2 move = new Vector2(-speed * Time.deltaTime, 0);
                Vector2 position = new Vector2(transform.position.x, transform.position.y);
                position += move;
                transform.position = position;
            }

            if (Input.GetKeyDown(KeyCode.Q)) //we are selecting and pressing the Q key
            {
                greenAlien.SetActive(false);//turns off the greenalien
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            //Allows the alien ship to move left when the leftArrow is pressed

            //2pm monday AZ Time
        }
    }
}
